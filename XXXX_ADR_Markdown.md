---
id: XXXX
title: Markdown-Format für ADRs
short_name: 
date: 29.05.2020
revision: 0
author:
  - Raphael Ahrens (raphael.ahrens fit.fraunhofer.de)
stakeholder: 
  - ADR-Prozess
  - Core-Team
  - Community
status: draft

---

# ADR: Markdown-Format für ADRs

Diese Dokument ist ein Vorschlag für das Format und die Struktur eines ADR.

# Kontext

Für den Prozess der Erstellung von ADs brauchen wir eine Format für die ADRs.
Das Format sollte die folgenden Eigenschaften haben

* Einfach zu erstellen
* Einfach zu versionieren
* in der Lage sein Metadaten zu enthalten
* Text zu strukturieren

## Markdown

[Markdown][md] ist eine Auszeichnungssprache, deren Quelltext einfacher zu erstellen und lesen als im Vergleich zu anderen Auszeichnungssprachen wie HTML, LaTex, MediaWiki oder Rich Text Format.
Ein Markdown-Quelltext lässt sich mit der Hilf eines Übersetznugssoftware in andere Auszeichnungssprachen übersetzen oder mit einer Anwendung direkt darstellen.

Markdown findet eine breite Unterstützung in der Entwicklergemeinde durch die direkte Unterstützung  in Anwendungen wie [GitHub][github], [GitLab][gitlab], [StackoverFlow][stackoverflow], [Doxygen][doxygen], [rustdoc][rustdoc] und [Jekyll][jekyll].

Zusätzlich gibt es noch Übersetzungssoftware wie [pandoc][pandoc], mit der sich Markdown-Quelltextdateien in andere Auszeichnungssprachen wie HTML und LaTex übersetzen lassen, aber auch direkt in andere Formate wie Office Open XML (.docx), OpenOffice text document (.odt) oder PDF.

Dadurch das Markdown Quelltext eine einfach Textdatei ist, lässt sich Markdown leicht mithilfe von Versionsverwaltungwerkzeugen, wie [Git][git], [Subversion][svn] und [Mecurial][hg] versionieren.
Zusätzlich könnten Werkzeuge wie GitLab auch für Verwaltung und die Bereitstellung der ADRs genutzt werden.


Ein Nachteil von Markdown ist das fehlen eines einheitlichen Standards, alles oben genanten Werkzeugen unterstützen ein gemeinsame Submenge an Funktionen, welche unter [John Gruber][md-syntax] zu erst definiert wurden und später als [CommonMark][cmd] definiert ist.
Aber viele Werkzeuge bieten weitere Funktionen an welche von den anderen nicht unterstütz werden oder anders umgesetzt werden.
Siehe hierzu [Github flavored Markdown][github], [GitLab Flavored Mardown][gitlab] und [Pandoc Markdown][pandoc].
Bei der Wahl der Werkzeuge für Markdown ist es also nötig zu beachten, welche Funktionen für die ADRs wichtig sind.
Im Grunde sollte die Funktionsmenge aus CommonMark ausreichen, aber Funktionen wie Metadaten wäre hilfreich das die Autoren Liste, die Stakeholder, Revisionsnummer und den Status.
GitLab, Pandoc und Jekyll bieten alle eine Metadaten-Unterstützung an, welche auf [YAML][yaml] basiert.
Hierzu wird am Anfang der Datei ein ein YAML-Block mit `---` eingeleitet und am Ende wieder mit `---` beendet.
Innerhalb des Blocks kann können in YAML formatierte Informationen stehen, welche von den Anwendungen Interpretiert werden können.
Die für die ADRs benötigten Metadaten könnte somit wie folgt aussehen

    ---
    id: 1234
    title: Schnittstelle zwischen UP und MP
    short_name: UP2MP
    date: 29.05.2020
    revision: 2
    author:
      - Max Muster (max@example.com)
      - Elfriede Example (e.example@example.com)
    stakeholder:
      - UP
      - MP
      - IPA
      - SAG
    status: draft
    ---

## Format der ADRs in Markdown

Damit die ADRs einheitlich sind muss eine grundlegenden Struktur festgelegt werden.
Hierfür gab es den Vorschlag dem TOGAF-Format zu folgen und folgenden Elemente in einem ADR abzulegen.

1. ID: Eine Nummer zu eindeutigen Identifizierung
1. Title: Der Title des ADR
1. Date: Das Datum wann es Erstellt wurde
1. Revision: Die Revisionsnummer beginnend bei 0
1. Author: List der Autoren des ADRs
1. Stakeholder: Liste der Involvierten Stakeholder
1. Status: Der Status des ADR (drafted, decide, accepted, rejected, deprecated, superseded, withdrawn)
1. Context: Der Kontext der für den ADR relevant ist
1. Decision: Die Entscheidung über den ADR
1. Consequences: Die Konsequenze die aus der Entscheidung folgen.

# Entscheidung

ADRs werden in Markdown verfasst und dem folgendermaßen strukturiert.


    ---
    id: <number>
    title: <text>
    date: <date in DD.MM.YYYY>
    revision: <number>
    author: <list of authors>
    stakeholder: <list of stakeholders>
    status: <drafted | decide | accepted | rejected | deprecated | superseded | withdrawn>
    ---

    # <Titel>
    
    <Abstract>

    # Kontext

    # Entscheidung

    # Konsequenz

    # Diskusion

# Konsequenzen

ADRs würden in Markdown mit der YAML-Metadatenerweiterung verfasst werden.
Damit liesen sich ADRs in GitLab verwalten und anzeigen, zusätzlich könnten mit Pandoc und Jekyll ADR-Dokumente erstellt werden.

# Diskussion


## Kommentar von Raphael Ahrens


### Vorteile

1. Einfach zu versionieren
1. Kann mit anderen Artefakten Versioniert werden
1. Die nötige Software zum bearbeiten und Umwandeln ist frei und kostenlos verfügbar.
1. ADRs liesen sich im zweifel ohne Probleme in ein anderes Format umwandeln wenn diese Entscheidung getroffen wird.

### Nachteile

1. das fehlen eines einheitlichen Standards.
2. Die Versionsverwaltung mit einem Werkzeug für Programmierern kann andere abschrecken.
   Dies ließe sich vielleicht mit Werkzeugen wie GitLab einfacher gestalten.

[md]:https://daringfireball.net/projects/markdown/ "Markdown"
[md-syntax]: https://daringfireball.net/projects/markdown/syntax "Markdown Syntax"
[cmd]: https://spec.commonmark.org/0.29/ "CommonMarkn"



[github]: https://github.github.com/gfm/ "GitHub flavored Markdown"
[gitlab]: https://docs.gitlab.com/ee/user/markdown.html "GitLab flavored Markdown"
[stackoverflow]: https://stackoverflow.com/editing-help "Stackoverflow Markdown Beschreibung"
[doxygen]: https://www.doxygen.nl/manual/markdown.html "Doxygen Markdown Unterstützung"
[rustdoc]: https://doc.rust-lang.org/rustdoc/how-to-write-documentation.html#markdown "rustdoc Mardown"
[jekyll]:https://jekyllrb.com/ "Jekyll static site Generator"
[pandoc]: https://pandoc.org/MANUAL.html#pandocs-markdown "pandoc Markdown Beschreibung"

[git]: https://git-scm.com/ "Git"
[hg]: https://www.mercurial-scm.org/ "Mecurial"
[svn]: https://subversion.apache.org/ "Subversion"

[yaml]: https://yaml.org/ "YAML"

